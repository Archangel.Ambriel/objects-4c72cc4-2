<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="description" content="FTPManager,Skyjos,ftp client"/>
  <meta name="viewport" content="width=device-width,user-scalable=yes,initial-scale=1.0">
  <title>FTPManager - Skyjos</title>
  
  <link rel="stylesheet" href="../../javascript/jquery/jquery.mobile-1.4.5.min.css">
  <link rel="stylesheet" href="../../css/help_style.css">
  <script src="../../javascript/jquery/jquery-1.11.2.min.js"></script>
  <script src="../../javascript/jquery/index.js"></script>
  <script src="../../javascript/jquery/jquery.mobile-1.4.5.min.js"></script>

  <script type="text/javascript">
  function getUrlParam(name) {
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r!=null) return unescape(r[2]); return null;
  }

  function showHelp(helpId) {
    var url = helpId + ".html";
    $('#sub_contents_in').load(url);
  }

  $( document ).on( "pagecreate", ".jqm-help", function( event ) {
    var page = $( this );
    $( ".jqm-navmenu-panel ul" ).listview();
    $( ".jqm-navmenu-link" ).on( "click", function() {
      page.find( ".jqm-navmenu-panel:not(.jqm-panel-page-nav)" ).panel( "open" );
    });
  });

  $(document).on("pagechange",function(event,data){
    var sectionName = getUrlParam('section');
	if (sectionName == null) {
		sectionName = "overview";
	}
    showHelp(sectionName);
  });
  </script>

</head>
<body>
  <div data-role="page" class="jqm-demos jqm-home">

    <div data-role="header" class="jqm-header">
    
        <div class="jqm_header_logo">
            <img height="50px" align="middle" src="../app_logo.png"/>
            <span style="display:inline-block; vertical-align:middle">
            <div class="app-title">FTPManager</div>
              <div class="app-subtitle">The FTP and SFTP Client</div>
            </span>
        </div>
      <a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
      <div id="header-line"></div>
    </div><!-- /header -->
    
    <div role="main" class="ui-content jqm-content">
      <div id="sub_contents_in" class="sub_contents_in"></div>
    </div><!-- /content -->

    <div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	    	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
          <li id="overview"><a href="help_ios.php?section=overview" data-ajax="false">Overview</a></li>
          
		      <li id='setup_connection'><a href="help_ios.php?section=setup_connection" data-ajax="false">Setup Connection</a></li>
          <li id='manage_connections'><a href="help_ios.php?section=manage_connections" data-ajax="false">Manage Connections</a></li>
          <li id='file_transfer'><a href="help_ios.php?section=file_transfer" data-ajax="false">File Transfer</a></li>
          <li id='device_transfer'><a href="help_ios.php?section=device_transfer" data-ajax="false">Nearby File Transfer</a></li>
          <li id='built-in_ftp_server'><a href="help_ios.php?section=built-in_ftp_server" data-ajax="false">Built-in FTP Server</a></li>
          <li id='configurations'><a href="help_ios.php?section=configurations" data-ajax="false">Distribute Configurations</a></li>
      </ul>
    </div>

  </div>


</body>

</html>
